import styled from "styled-components";

export const ContainerCss = styled.div`
  color: white;
  .my-swipeable-view:first-child {
    background: blue;
  }
  .my-swipeable-view:last-child {
    background: red;
  }

  .slide {
    height: 300px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;
