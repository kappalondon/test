import React, { useState } from "react";
import SwipeableViews from "react-swipeable-views";
import { ContainerCss } from "./styles.css";

const Problem = () => {
  const [activeTab, setActiveTab] = useState(0);
  return (
    <ContainerCss>
      <div>
        <button onClick={() => setActiveTab(0)}>1</button>
        <button onClick={() => setActiveTab(1)}>2</button>
      </div>
      <SwipeableViews
        slideClassName="my-swipeable-view"
        index={activeTab}
        enableMouseEvents={false}
        onChangeIndex={(index) => setActiveTab(index)}
      >
        <div className="slide">slide n°1</div>
        <div className="slide">slide n°2</div>
      </SwipeableViews>
    </ContainerCss>
  );
};

export default Problem;
